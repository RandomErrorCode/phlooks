## 1.0 (2021-01-01)

Rewrote sections, added support for additional distributions and compatibility with BusyBox

**Implemented:**

- Add compatibility for Arch Linux, Fedora, Manjaro, openSUSE and postmarketOS
- Adjust how themes are displayed to add support for BusyBox
- Add additional improvements for displaying themes
- Improve test expressions to be more aesthetic
- Add additional tests to prevent running commands on non-existing files
- Disable user theme extension when choosing the default GNOME Shell theme
- Update README.md

## 0.4 (2020-12-24)

Added features and improvements

**Implemented:**

- Add distinction between Phone shell and GNOME Shell features 
- Improve installing and removing themes
- Improve readability in gedit for dark themes
- Create README.md
- Add '--help' and '--version' command option

## 0.3 (2020-12-23)

Added feature

**Implemented:**

- Add functionality to remove user themes

## 0.2 (2020-12-20)

Added feature

**Implemented:**

- Add functionality to install user themes

## 0.1 (2020-12-08)

Initial release

**Implemented:**

- Basic functionality for changing themes on Mobian

