# PhLooks

A tool to manage user themes for Phosh (Phone shell) and GNOME.
PhLooks lets you customize mobile and desktop user environments based on the GNOME stack by invoking native setting options (gsettings).
You can set, install and remove GTK, icon and shell themes as well as wallpapers or desktop backgrounds.

## Get PhLooks

Open a terminal and enter:

```
$ git clone https://gitlab.com/RandomErrorCode/phlooks.git
```

This will download PhLooks to your current work directory.

## Use PhLooks

In a terminal, go into the project folder and run the tool with:

```
$ cd phlooks/
$ ./phlooks
```

You can select menu entries by entering the respective menu number.
To select a theme, just enter its name.
By entering 'q' at any prompt you can quit the tool.

To be able to install themes to your home directory, you have to place them inside one of the dedicated subfolders in the "data" directory.

## Additional command line options

**Desktop mode**

Manage themes and toggle shell theme extension for the GNOME desktop environment.

```
$ ./phlooks -d
```

```
$ ./phlooks --desktop
```

**Display help**

Display a help text and get information about command options.

```
$ ./phlooks --help
```

**Display version**

Display the version number, source repository and license.

```
$ ./phlooks --version
```

## Compatibility

PhLooks was primarily written for Mobian, which is using the Phone Shell (Phosh), but also supports other PinePhone distributions and works with the GNOME DE on PC, e.g. on Debian.

**Tested PinePhone distributions (with Phosh):**

- Arch Linux ARM        (build 2020-12-23)
- Fedora Rawhide        (build 2020-12-23)
- Manjaro ARM           (build 2020-12-22)
- Mobian                (build 2020-12-22)
- openSUSE Tumbleweed   (build 2020-12-22)
- postmarketOS          (build 2020-12-23)

